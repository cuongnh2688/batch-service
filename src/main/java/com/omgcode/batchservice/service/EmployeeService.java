package com.omgcode.batchservice.service;

import com.omgcode.batchservice.dto.EmployeeDTO;
import com.omgcode.batchservice.entity.Employee;
import com.omgcode.batchservice.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;


    public void persitEmployee(EmployeeDTO employeeDTO) {
        this.employeeRepository.save(
                Employee.builder()
                        .code(employeeDTO.getCode())
                        .name(employeeDTO.getName())
                        .note(employeeDTO.getNote())
                        .build());
    }
}
