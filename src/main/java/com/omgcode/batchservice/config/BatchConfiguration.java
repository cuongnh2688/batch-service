package com.omgcode.batchservice.config;


import com.omgcode.batchservice.entity.OrderItem;
import com.omgcode.batchservice.job.JobCompletionNotificationListener;
import com.omgcode.batchservice.repository.OrderRepository;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Autowired
    public JobCompletionNotificationListener jobCompletionNotificationListener;
    @Bean
    public ItemReader<OrderItem> reader() {
        String[] header = new String[] {"id","customerId", "itemId", "itemPrice", "itemName", "purchaseDate"};

        // Mapping with the order entity
        BeanWrapperFieldSetMapper<OrderItem> beanWrapperFieldSetMapper
                = new BeanWrapperFieldSetMapper<OrderItem>();
        beanWrapperFieldSetMapper.setTargetType(OrderItem.class);

        // Read the file CSV resource
        FlatFileItemReader<OrderItem> orderReadItem = new FlatFileItemReaderBuilder<OrderItem>()
                .name("orderReadItem")
                .resource(new ClassPathResource("/cvs/order-item.csv"))
                .linesToSkip(1)
                .delimited()
                .names(header)
                .fieldSetMapper(beanWrapperFieldSetMapper)
                .build();
        return orderReadItem;
    }

    @Bean
    public ItemProcessor<OrderItem, OrderItem> processor(){
        return new ItemProcessor<OrderItem, OrderItem>() {

            @Override
            public OrderItem process(OrderItem order) throws Exception {
                return order;
            }
        };
    }

    @Bean
    public ItemWriter<OrderItem> writer() {
        RepositoryItemWriter<OrderItem> writer = new RepositoryItemWriter<>();
        writer.setRepository(orderRepository);
        writer.setMethodName("save");
        return writer;
    }

    @Bean
    public Step step1() {
        return  this.stepBuilderFactory.get("Step 1")
                .<OrderItem, OrderItem>chunk(10)
                .reader(reader())
                .processor(processor())
                .writer(writer())
                .build();
    }

    @Bean
    public Job importOrderJob() {
       return this.jobBuilderFactory
               .get("importOrderJob")
               .incrementer(new RunIdIncrementer())
               .listener(this.jobCompletionNotificationListener)
               .flow(step1())
               .end()
               .build();
    }
}
