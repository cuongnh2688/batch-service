package com.omgcode.batchservice.controller;


import com.omgcode.batchservice.dto.EmployeeDTO;
import com.omgcode.batchservice.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @PostMapping("/create")
    public ResponseEntity<String> createEmployee(@RequestBody EmployeeDTO employeeDTO){
        this.employeeService.persitEmployee(employeeDTO);
        return ResponseEntity.ok("The Employee is created successfully!");
    }
}
